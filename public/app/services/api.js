/*global FoorumApp  */
FoorumApp.service('Api', function($http){
	
	// Aihealueiden Api-funktiot
	this.getTopics = function(){
		// Hae kaikki aihealueet toteuttamasi Api:n polusta /topics
		console.log('Haetaan kaikki topicit');

		return $http.get('/topics').success(function(data, status, headers, config){
			console.log('Topicit haettu');
		})
		.error(function(data, status, headers, config){
			console.log('Topicit EI haettu');
			console.log(data);
		});
	}
	
	this.getTopic = function(id){
		// Hae annetulla id:llä varastettu aihealue toteuttamasi Api:n polusta /topics/:id
		console.log('Haetaan topici id:llä :'+id);
		
		return $http.get('/topics/'+id).success(function(data, status, headers, config){
			console.log('Topic haettu');
		})
		.error(function(data, status, headers, config){
			console.log('Topic EI haettu');
			console.log(data);
		});
	}
	
	this.addTopic = function(topic){
		// Lisää annettu aihealue lähettämällä POST-pyyntö toteuttamasi Api:n polkuun /topics
		console.log('Postataan topic');
		
		return $http.post('/topics', { name:topic.name, description: topic.description }).success(function(data, status, headers, config){
			console.log('Topic postattu');
		})
		.error(function(data, status, headers, config){
			console.log('Topic EI postattu');
			console.log(data);
		});
	}
	
	// Viestien Api-funktiot
	this.getMessages = function(id){
		// Hae kaikki viestit polusta /messages
		console.log('Haetaan kaikki Topic viestit');
		
		return $http.get('/topics/'+id+'/messages').success(function(data, status, headers, config){
			console.log('Topic Viestit haettu');
		})
		.error(function(data, status, headers, config){
			console.log('Topic Viestit EI haettu');
			console.log(data);
		});
		
	}

	// Viestien Api-funktiot
	this.getMessage = function(id){
		// Hae annetulla id:llä varustettu viesti toteuttamasi Api:n polusta /messages/:id
		console.log('Haetaan viesti id:llä: '+id);
		
		return $http.get('/messages/'+id).success(function(data, status, headers, config){
			console.log('Viesti haettu');
		})
		.error(function(data, status, headers, config){
			console.log('Viesti EI haettu');
			console.log(data);
		});
		
	}
	this.addMessage = function(message, topicId){
		// Lisää annettu viesti lähettämällä POST-pyyntö toteuttamasi Api:n polkuun /topics/:topicId/message
		console.log('Postataan viesti');
		
		return $http.post('/topics/'+topicId+'/message', { title: message.title, content: message.content }).success(function(data, status, headers, config){
			console.log('Viesti postattu');
		})
		.error(function(data, status, headers, config){
			console.log('Viesti EI postattu');
			console.log(data);
		});
	}

	// Vastausten Api-funktiot
	this.addReply = function(reply, messageId){
		// Lisää annettu vastaus lähettämällä POST-pyyntö toteuttamasi Api:n polkuun /messages/:messageId/reply
		console.log('Postataan Reply-viesti Viestille: '+messageId);
		
		return $http.post('/messages/'+messageId+'/reply', { content: reply.content }).success(function(data, status, headers, config){
			console.log('Reply-viesti postattu');
		})
		.error(function(data, status, headers, config){
			console.log('Reply-viesti EI postattu');
			console.log(data);
		});
	}

	// Käyttäjän Api-funktiot
	this.login = function(user){
		// Tarkista käyttäjän kirjautuminen lähettämällä POST-pyyntö toteuttamasi Api:n polkuun /users/authenticate
		console.log('Kirjaudutaan sisään käyttäjätunnuksella: '+user.username);
		return $http.post('/users/authenticate', user).success(function(data, status, headers, config){
			console.log('Kirjautuminen onnistui');
		})
		.error(function(data, status, headers, config){
			console.log('Kirjautuminen EI onnistunut');
			console.log(data);
		});
	}
	
	
	this.register = function(user){
		// Lisää annettu käyttäjä lähettämällä POST-pyyntö toteuttamasi Api:n polkuun /users
		console.log('Luodaan uusi käyttäjä: '+user.username);
		return $http.post('/users/', user).success(function(data, status, headers, config){
			console.log('Kirjautuminen onnistui');
		})
		.error(function(data, status, headers, config){
			console.log('Kirjautuminen EI onnistunut');
			console.log(data);
		});
	}
	
	
	this.getUserLoggedIn = function(){
		// Hae kirjautunut käyttäjä toteuttamasi Api:n polusta /users/logged-in
		console.log('Haetaan kirjautunut käyttäjä');
		return $http.get('/users/logged-in').success(function(data, status, headers, config){
			console.log('Kirjautunut käyttäjä on: ' + data.username);
		})
		.error(function(data, status, headers, config){
			console.log(data);
		});
	}
	
	// this.getUserName = function(id){
	// 	// Hae kirjautunut käyttäjä toteuttamasi Api:n polusta /users/logged-in
	// 	console.log('Haetaan käyttäjätunnus id:llä: '+id);
	// 	return $http.get('/users/'+id).success(function(data, status, headers, config){
	// 		console.log('Kkäyttäjätunnus on: ' + data);
	// 	})
	// 	.error(function(data, status, headers, config){
	// 		console.log(data);
	// 	});
	// }
	
	
	this.logout = function(){
		return $http.get('/users/logout');
	}
});
