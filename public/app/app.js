/*global angular  */
var FoorumApp = angular.module('FoorumApp', ['ngRoute']);

//		https://sovellusfoorumi-grigorijs.c9users.io

FoorumApp.config(function($routeProvider){
	$routeProvider
		.when('/', {
			controller: 'TopicsListController',
			templateUrl: 'app/views/topics/index.html',
			resolve: {
				userLoggedIn: function($rootScope, $location, Api){
					return Api.getUserLoggedIn()
					.success(function(user){
						$rootScope.userLoggedIn = user.username ? user : null;
					}).error(function() {
						$location.path('/login')
					});
				}
			}
		})
		.when('/topics/:id/messages', {
			controller: 'ShowTopicMessagesController',
			templateUrl: 'app/views/topics/show.html',
			resolve: {
				userLoggedIn: function($rootScope, $location, Api){
					return Api.getUserLoggedIn()
					.success(function(user){
						$rootScope.userLoggedIn = user.username ? user : null;
					}).error(function() {
						$location.path('/login')
					});
				}
			}
		})
		.when('/messages/:id', {
			controller: 'ShowMessageController',
			templateUrl: 'app/views/messages/show.html',
			resolve: {
				userLoggedIn: function($rootScope, $location, Api){
					return Api.getUserLoggedIn()
					.success(function(user){
						$rootScope.userLoggedIn = user.username ? user : null;
					}).error(function() {
						$location.path('/login')
					});
				}
			}
		})
		.when('/login', {
			controller: 'UsersController',
			templateUrl: 'app/views/users/login.html',
			// resolve: {
			// 	userLoggedIn: function($rootScope, $location, Api){
			// 		return Api.getUserLoggedIn().success(function(user){
			// 			$rootScope.userLoggedIn = user.username ? user : null;
			// 			$location.path('/')
			// 		}).error(function() {
			// 			console.log('/login    JOKU VIRHE')
			// 			$location.path('/login')
			// 		});
			// 	}
			// }
		})
		.when('/register', {
			controller: 'UsersController',
			templateUrl: 'app/views/users/register.html',
			// resolve: {
			// 	userLoggedIn: function($rootScope, $location, Api){
			// 		return Api.getUserLoggedIn().success(function(user){
			// 			$rootScope.userLoggedIn = user.username ? user : null;
			// 			$location.path('/')
			// 		}).error(function() {
			// 			console.log('/register    JOKU VIRHE')
			// 			$location.path('/register')
			// 		});
			// 	}
			// }
		})
		.otherwise({
			redirectTo: '/'
		});
});

FoorumApp.run(function($rootScope, $location, Api){
	$rootScope.logOut = function(){
		Api.logout().success(function(){
			$location.path('/login');
			$rootScope.userLoggedIn = null;
		});
	}
});