/*global FoorumApp  */
FoorumApp.controller('ShowMessageController', function($scope, $routeParams, Api){
  // Toteuta kontrolleri tähän
  
  Api.getMessage($routeParams.id).success(function(message){
      $scope.Message = message
      
      
      if (message.Replies != undefined) {
          $scope.replyCount = (message.Replies.length == 1) ? message.Replies.length + ' vastaus' : message.Replies.length + ' vastausta'
          $scope.replies = message.Replies
      }
  })
  
  $scope.addReply = function() {
	  Api.addReply({ content: $scope.newReply.content }, $routeParams.id)
		.success(function(topic){
			console.log('uusi topic lisätty')
			location.reload();
		})
  }
});
