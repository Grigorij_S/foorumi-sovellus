/*global FoorumApp  */
FoorumApp.controller('TopicsListController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän


    Api.getTopics().success(function(topics) {
        $scope.Topics = topics;
    });
  
  
    $scope.addTopic = function() {
        Api.addTopic({ name: $scope.newTopic.name, description: $scope.newTopic.description })
            .success(function(topic){
                console.log('uusi topic lisätty')
                location.reload();
            })
    }
});
