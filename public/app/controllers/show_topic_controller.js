/*global FoorumApp  */
FoorumApp.controller('ShowTopicMessagesController', function($scope, $routeParams, $location, Api){
  // Toteuta kontrolleri tähän
  
  Api.getMessages($routeParams.id).success(function(messages) {
		$scope.Messages = messages;
		$scope.messageCount = (messages.length == 1) ? messages.length + ' viesti' : messages.length + ' viestiä'
  });
  
  
  Api.getTopic($routeParams.id).success(function(topic) {
		$scope.topic = topic;
  });
  

  $scope.addMessage = function() {
	  Api.addMessage({ title: $scope.newMessage.title, content: $scope.newMessage.content }, $routeParams.id)
		.success(function(){
			console.log('uusi viesti lisätty')
			location.reload();
		})
  }
});