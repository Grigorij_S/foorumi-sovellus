/*global FoorumApp  */
FoorumApp.controller('UsersController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän
  
	$scope.register = function() {
		
		$scope.regError = ""
		
		if ($scope.register.password == null || $scope.register.password == "") {
			$scope.regError = "Input password!"
			return;
		}
		
		if ($scope.register.password != $scope.register.password2) {
			$scope.regError = "Passwords don't match!"
			return;
		}
		
		if ($scope.register.username == null || $scope.register.username == "") {
			$scope.regError = "Input username!"
			return;
		}
		
		Api.register({ username: $scope.register.username, password: $scope.register.password })
		.success(function(user){
			console.log('Rekisteröitynyt onnistuneesti')
			Api.login({ username: $scope.register.username, password: $scope.register.password })
				.success(function(user){
					console.log('Logged in')
					$location.path('/app/views/topics/index.html')
				})
		}).error(function() {
			$scope.regError = "Username already taken"
		})
	}
  
  
	$scope.logIn = function() {
		Api.login({ username: $scope.login.username, password: $scope.login.password })
		.success(function(user){
			console.log('Logged in')
			$location.path('/app/views/topics/index.html')
		})
		.error(function() {
			$scope.errorMessage = 'Väärä käyttäjätunnus tai salasana!';
		})
	}
});
