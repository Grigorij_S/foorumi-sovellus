var express = require('express');
var router = express.Router();

var authentication = require('../utils/authentication');
var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /topics

// GET /topics
router.get('/', function(req, res, next) {
	Models.Topic.findAll().then(function(topics){
		// res.status(200).json(topics)
		res.send(topics)
	});
});

// GET /topics/:id
router.get('/:id', function(req, res, next) {
	Models.Topic.findOne({
		where: {
			id: req.params.id
		},
		include: {
			model: Models.Message,
			include: {
				model: Models.User
			}
		}
	}).then(function(topic){
	 	res.send(topic)
	});
});



//TEST	TEST	TEST	TEST	TEST
// HAE KAIKKI VIESTIT JOTKA KUULUVAT TIETTYYN TOPICCIIN
// GET /topics/:id/messages
router.get('/:id/messages', function(req, res, next) {
	Models.Message.findAll({ where: {
		TopicId: req.params.id 
	},
	include: {
		model: Models.User
			}
	}).then(function(messages){
		if (messages == null) {
			res.status(404).json({ error: 'Messages for topic : ' + req.params.id + ' not found.'})
			res.send()
		}else {
			// res.status(200).json(messages)
			res.send(messages)
		}
	});
});

//TEST	TEST	TEST	TEST	TEST




// POST /topics
router.post('/', authentication, function(req, res, next) {
	Models.Topic.create({
		name: req.body.name,
		description: req.body.description
	}).then(function(topic){
		res.status(201).json('Topic : ' + topic.name + ' successfully added.')
		res.send()
	});
});

// POST /topics/:id/message
router.post('/:id/message', authentication, function(req, res, next) {
	Models.Topic.findOne({ where: { id: req.params.id } }).then(function(topic){
		if (topic == null) {
			res.status(404).jsonp({ error: 'Topic : ' + req.params.id + ' not found.'})
			res.send()
		}else {
			Models.Message.create({ 
				title: req.body.title,
				content: req.body.content,
				TopicId: req.params.id,
				UserId: req.session.userId
			})
			res.status(201).json({ message: 'Message : ' + req.body.title + ' successfully added to Topic: ' + req.params.id})
			res.send()
		}
	});
});

// DELETE /topics/:id
router.delete('/:id', function(req, res, next) {
	Models.Topic.destroy({where: {id: req.params.id}}).then(function(id){
		if (id == 0) {
			res.status(404).json({ error: 'Topic :' + req.params.id + ' not found.'})
			res.send()
		}else {
			res.status(200).json({ message: 'Topic :' + req.params.id + ' successfully removed.'})
			res.send()
		}
	})
});

module.exports = router;
