var express = require('express');
var router = express.Router();

var authentication = require('../utils/authentication');
var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /messages

// GET /messages
router.get('/', function(req, res, next) {
	Models.Message.findAll().then(function(messages){
		// res.status(200).jsonp(messages)
		res.send(messages)
	});
});


// GET /messages/:id
router.get('/:id', function(req, res, next) {
	// Hae viesti tällä id:llä ja siihen liittyvät vastaukset tässä (Vinkki: findOne ja sopiva include)
	Models.Message.findOne({ 
		where: { 
			id: req.params.id 
		}, 
		include: {
			model : Models.Reply,
			include: {
				model : Models.User 
			}
		}
	}).then(function(message){
		if (message == null) {
			// res.status(404).jsonp({ error: 'Message : ' + req.params.id + ' not found.'})
			res.send({ error: 'Message : ' + req.params.id + ' not found.'})
		}else {
			// res.status(200).jsonp(message)
			res.send(message)
		}
	});
});

// POST /messages/:id/reply
router.post('/:id/reply', authentication, function(req, res, next){
	// Lisää tällä id:llä varustettuun viestiin tämä vastaus (Vinkki: lisää ensin replyToAdd-objektiin kenttä MessageId, jonka arvo on messageId-muuttujan arvo ja käytä sen jälkeen create-funktiota)
	Models.Message.findOne({ where: { id: req.params.id } }).then(function(message){
		if (message == null) {
			// res.status(404).jsonp({ error: 'Message : ' + req.params.id + ' not found.'})
			res.send({ error: 'Message : ' + req.params.id + ' not found.'})
		}else {
			Models.Reply.create({ 
				content : req.body.content,
				MessageId : req.params.id,
				UserId : req.session.userId
			})
			// res.status(200).jsonp({ message: 'Reply : ' + req.body.content + ' successfully added to Message: ' + req.params.id})
			res.send({ message: 'Reply : ' + req.body.content + ' successfully added to Message: ' + req.params.id})
		}
	});
});


// DELETE /message/:id
router.delete('/:id', function(req, res, next) {
	Models.Message.destroy({where: {id: req.params.id}}).then(function(id){
		if (id == 0) {
			// res.status(404).jsonp({ error: 'Message :' + req.params.id + ' not found.'})
			res.send({ error: 'Message :' + req.params.id + ' not found.'})
		}else {
			// res.status(200).jsonp({ message: 'Message :' + req.params.id + ' successfully removed.'})
			res.send({ message: 'Message :' + req.params.id + ' successfully removed.'})
		}
	});
});

module.exports = router;
