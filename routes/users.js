var express = require('express');
var router = express.Router();

var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /users


router.get('/', function(req, res, next){
	Models.User.findAll().then(function(users){
		res.send(users)
	});
});

// POST /users
router.post('/', function(req, res, next){
	// Lisää tämä käyttäjä (Vinkki: create), muista kuitenkin sitä ennen varmistaa, että käyttäjänimi ei ole jo käytössä! (Vinkki: findOne)
	Models.User.findOne({ where: { username: req.body.username } }).then(function(user){
		if (user == null) {
			Models.User.create({ 
				username: req.body.username,
				password: req.body.password
			}).then(function(user){
				// res.status(200).jsonp(user)
				res.send(user)
			});
		}else {
			// res.status(400).json({ error: 'Käyttäjätunnus on jo käytössä!' });
			res.send(400)
		}
		
	});
});

// POST /users/authenticate
router.post('/authenticate', function(req, res, next){
	// Tarkista käyttäjän kirjautuminen tässä. Tee se katsomalla, löytyykö käyttäjää annetulla käyttäjätunnuksella ja salasanalla (Vinkki: findOne ja sopiva where)
	var userToCheck = req.body;
	if(userToCheck == null || userToCheck.username == null || userToCheck.password == null){
		res.send(403);
	}
	Models.User.findOne({
		where: {
		username: userToCheck.username,
		password: userToCheck.password
	}}).then(function(user){
		if(user){
			req.session.userId = user.id;
			res.json(user)
		}else{
			res.sendStatus(403);  // konsoli suositteli
			// res.send(403);
		}
 });
});

// GET /users/logged-in
router.get('/logged-in', function(req, res, next){
	var loggedInId = req.session.userId ? req.session.userId : null;

	if(loggedInId == null){
		// res.json({error: 'ei kirjautunut'});
		// res.status(400).json({ error: 'Kukaan ei ole kirjautunt' });
		res.send(400)
	}else{
		// Hae käyttäjä loggedInId-muuttujan arvon perusteella (Vinkki: findOne)
		Models.User.findOne({ where: { id: loggedInId } }).then(function(user){
			// res.status(200).json(user);
			res.send(user)
		});
	}
});

// GET /users/logout
router.get('/logout', function(req, res, next){
	req.session.userId = null;
	res.send(200);
});


router.delete('/:id', function(req, res, next){
	Models.User.destroy({where: {id: req.params.id}}).then(function(user){
		console.log('User with id: '+req.params.id+' was deleted')
		res.send(200);
	})
});

module.exports = router;
