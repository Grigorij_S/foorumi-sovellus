const frisby = require('frisby');
const Joi = frisby.Joi; // Frisby exports Joi for convenience on type assersions


//          ./node_modules/.bin/jest

//          https://sovellusfoorumi-grigorijs.c9users.io


it ('1. should return status 200', function (done) {
  frisby
    .get('https://sovellusfoorumi-grigorijs.c9users.io/topics')
    .expect('status', 200)
    .done(done);
});

it ('2. should return status 200 and all topics', function (done) {
  frisby
    .get('https://sovellusfoorumi-grigorijs.c9users.io/topics')
    .expect('status', 200)
    .expect('jsonTypes', '*', {
      'id': Joi.number().required(),
      'name': Joi.string().required(),
      'description': Joi.required(),
    })
    .done(done);
});

it ('3. should return status 200 and topic name should be Topic1 where index is 1', function (done) {
  frisby
    .get('https://sovellusfoorumi-grigorijs.c9users.io/topics/1')
    .expect('status', 200)
    .expect('jsonTypes', '', {
      'id': Joi.number().required(),
      'name': "Topic1",
      'description': Joi.required(),
    })
    .done(done);
});

